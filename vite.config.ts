import {defineConfig} from 'vite';
import vue from '@vitejs/plugin-vue';

// https://vitejs.dev/config/
export default defineConfig(({command}) => {
    const ret = {plugins: [vue()], base: '/raetsel/'};
    if (command === "build") {
        ret.base = '/static/';
    }
    return ret;
});
