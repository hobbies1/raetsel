# Rätsel

This is a simple webpage for playing crossword puzzles in the browser. A german newspaper has some crosswords I really enjoy, but I dislike their online interface. Hence I implemented something on my own. When programming, I default to writing english, but the puzzles are on german, hence parts of the project have some mixed language.

The crosswords I refer to are subscription based and not mine, hence I cannot share them here. But you can input any crosswords you want in the format described in `crossword_json_format.txt`. If you have a source for interesting puzzles that uses a different format, feel free to open an issue or a pull request for a translator.
