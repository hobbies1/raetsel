import {Position, Size} from "./base_types";

export function twod_array<T>(pos: Position): (T[])[] {
    const ret = new Array(pos.x);
    for (let x = 0; x < pos.x; ++x) {
        ret[x] = new Array(pos.y);
    }
    return ret;
}

export function get_twod<T>(arr: T[][], pos: Position): T | undefined {
    return arr[pos.x]?.[pos.y];
}

export function set_twod<T>(arr: T[][], pos: Position, value: T): void {
    arr[pos.x][pos.y] = value;
}

export function in_rect({x, y}: Position, {x: left, y: top}: Position, {width, height}: Size): boolean {
    return left <= x && x < left + width && top <= y && y < top + height;
}

const answer_regex = /(?<result>(?:[A-Z]-?){2,})/g;

export function extract_answer(desc: string | undefined): string | undefined {
    const candidates = desc?.matchAll(answer_regex) ?? [];
    let ret: string | undefined = undefined;
    for (const cand of candidates) {
        const result = cand.groups?.['result'];
        if (!result) {
            continue;
        }

        ret = result.replaceAll('-', '');
    }
    return ret;
}

export function find_number(): number {
    const regex = /\/raetsel\/(\d+)/;
    console.log(regex, window.location.pathname);
    const result = window.location.pathname.match(regex);
    if (result) {
        return Number(result[1]);
    }
    // Standard for testing.
    return 852;
}
