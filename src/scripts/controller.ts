import { Position, QuestionIdentifier } from "./base_types";
import { Puzzle } from "./puzzle";
import { get_twod, set_twod, twod_array } from "./helper";

export type event = 'ARROW' | 'LETTER' | 'CLEAR' | 'BACK' | 'FORWARD' | 'CHANGE_DIRECTION';
type arrow_direction = 'left' | 'right' | 'up' | 'down'

export class Controller {
    focused: Position | undefined;
    puzzle: Puzzle;
    direction: 'horizontal' | 'vertical' = 'horizontal';
    highlighted_start: Position | undefined;
    highlighted_end: Position | undefined;
    private readonly emitter: (event: string, qid: QuestionIdentifier) => void;
    private readonly content: string[][];
    private readonly storage_key: string;

    constructor(puzzle: Puzzle, emitter: (_0: string, _1: QuestionIdentifier) => void,
        qid: QuestionIdentifier | undefined) {
        this.puzzle = puzzle;
        this.storage_key = `kreuzwort_${this.puzzle.num}`;
        this.content = this.get_stored() ?? twod_array(this.puzzle.max);
        this.emitter = emitter;
        this.update_from_qid(qid);
    }

    update_from_qid(qid: QuestionIdentifier | undefined): void {
        if (!qid) {
            return;
        }
        this.direction = qid.direction;
        const pos = this.puzzle[qid.direction][qid.key].pos;
        this.focus_elem(pos, true);
    }

    append_content(pos: Position, value: string | undefined): void {
        const content = this.get_content(pos) ?? '';
        this.set_content(pos, content + value);
    }

    set_content(pos: Position, value: string | undefined): void {
        set_twod(this.content, pos, value ?? '');
        this.store();
    }

    get_content(pos: Position): string | undefined {
        return get_twod(this.content, pos);
    }

    private get_stored(): string[][] | undefined {
        const stored = window.localStorage.getItem(this.storage_key);
        if (stored === null) {
            return undefined;
        } else {
            return JSON.parse(stored) as string[][];
        }
    }

    private store(): void {
        window.localStorage.setItem(this.storage_key, JSON.stringify(this.content));
    }

    * xrange(): Generator<number, void> {
        for (let x = 0; x < this.puzzle.max.x; ++x) {
            yield x;
        }
    }

    * yrange(): Generator<number, void> {
        for (let y = 0; y < this.puzzle.max.y; ++y) {
            yield y;
        }
    }

    handle_event(ev: event, ...args: string[]): void {
        if (this.focused === undefined) {
            return;
        }
        switch (ev) {
            case "BACK":
                const content = this.get_content(this.focused);
                if (this.puzzle.is_var_length(this.focused) && content) {
                    this.set_content(this.focused, content.slice(0, -1));
                } else {
                    this.go_back();
                    this.clear();
                }
                break;
            case "CLEAR":
                this.clear();
                break;
            case "ARROW":
                this.arrow(args[0] as arrow_direction);
                break;
            case "FORWARD":
                this.go_forward();
                break;
            case "LETTER":
                if (this.puzzle.is_var_length(this.focused)) {
                    this.append_content(this.focused, args[0]);
                } else {
                    this.set_content(this.focused, args[0]);
                    this.go_forward();
                }
                break;
            case "CHANGE_DIRECTION":
                this.toggle_direction();
                this.update_highlight()
                break;
        }
    }

    update_highlight(keep_direction = true): void {
        if (!this.focused) {
            return;
        }
        const focused_field = this.puzzle.get_field(this.focused);
        if (focused_field === undefined) {
            return;
        }
        if (!keep_direction && focused_field.key !== undefined &&
            !this.puzzle[this.direction].hasOwnProperty(focused_field.key)) {
            this.toggle_direction();
        }

        let found = this.puzzle.get_key(this.focused, this.direction);
        if (found === undefined) {
            this.toggle_direction();
            found = this.puzzle.get_key(this.focused, this.direction);
            if (found === undefined) {
                return;
            }
        }
        const [key, pos] = found;
        this.highlight_question(key, pos);

        this.emitter("focus", { direction: this.direction, key });
    }

    private highlight_question(key: number, pos: Position) {

        const length = this.puzzle[this.direction][key].length - 1;
        this.highlighted_start = pos;
        if (this.direction === 'horizontal') {
            this.highlighted_end = { x: pos.x + length, y: pos.y };
        } else {
            this.highlighted_end = { x: pos.x, y: pos.y + length };
        }
        return key;
    }

    is_focussy(pos: Position): boolean {
        if (!this.highlighted_start || !this.highlighted_end) {
            return false;
        }
        if (this.direction === 'horizontal') {
            return (this.highlighted_start.y === pos.y) && (this.highlighted_start.x <= pos.x) &&
                (pos.x <= this.highlighted_end.x);
        } else {
            return (this.highlighted_start.x === pos.x) && (this.highlighted_start.y <= pos.y) &&
                (pos.y <= this.highlighted_end.y);
        }
    }

    is_focused(pos: Position): boolean {
        return pos.x === this.focused?.x && pos.y === this.focused?.y;
    }

    private focus_elem(pos: Position, force_highlight = false): void {
        this.focused = pos;
        document.getElementById(this.get_id(this.focused))?.focus();
        if (!this.is_focussy(pos) || force_highlight) {
            this.update_highlight(!force_highlight);
        }
    }


    get_id({ x, y }: Position): string {
        // Usability with vue is worse if this is static.
        return `x:${x},y:${y}`;
    }


    private arrow(dir: arrow_direction): void {
        if (!this.focused) {
            return;
        }
        let next;
        const { x, y } = this.focused;

        switch (dir) {
            case 'down':
                next = { x, y: y + 1 };
                break;
            case 'up':
                next = { x, y: y - 1 };
                break;
            case 'left':
                next = { x: x - 1, y };
                break;
            case 'right': {
                let diff = 1;
                const containing = this.puzzle.get_containing_pic(this.focused);
                if (containing) {
                    diff = containing.size.width;
                }
                next = { x: x + diff, y };
                break;
            }
        }
        if (this.puzzle.get_field(next)) {
            const containing = this.puzzle.get_containing_pic(next);
            if (containing) {
                next.x = containing.position.x;
            }
            this.focus_elem(next);
        }
    }


    private go_forward(): void {
        if (this.direction === 'horizontal') {
            this.arrow('right');
        } else {
            this.arrow('down');
        }
    }


    private clear(pos: Position | undefined = this.focused): void {
        if (pos === undefined) {
            return;
        }
        this.set_content(pos, undefined);
    }


    private go_back(): void {
        if (this.direction === 'horizontal') {
            this.arrow('left');
        } else {
            this.arrow('up');
        }
    }

    private toggle_direction(): void {
        if (this.direction === 'horizontal') {
            this.direction = 'vertical';
        } else {
            this.direction = 'horizontal';
        }
    }


    click(pos: Position): void {
        if (this.is_focused(pos)) {
            this.toggle_direction();
            this.update_highlight();
        } else {
            this.focus_elem(pos, true);
        }
    }

    get_correctness(pos: Position): "unknown" | "correct" | "incorrect" {
        const solution = get_twod(this.puzzle.solution, pos)
        const content = get_twod(this.content, pos)
        if (!solution || !content) {
            return "unknown"
        }
        if (content === solution) {
            return "correct"
        }
        return "incorrect"
    }
}
