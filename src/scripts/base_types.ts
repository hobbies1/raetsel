export interface Picture {
    url: string
    position: Position
    size: Size
}

export interface PreQuestion {
    question: string
    length: number
    answer?: string
    description: string
    picture?: Picture
}

export interface Question extends PreQuestion {
    key: number
    pos: Position
}

export interface Position {
    x: number,
    y: number
}

export interface Size {
    width: number
    height: number
}

export interface Entry {
    vertical?: PreQuestion
    horizontal?: PreQuestion
    key: number
    position: Position
}

export interface Reply {
    meta: Record<string, string>
    questions: Entry[]
    image: Picture | undefined
}

const borders = {top: 'top', bottom: 'bottom', left: 'left', right: 'right'} as const;
export type PreField = {
    -readonly [Property in keyof typeof borders]: boolean;
}

export interface Field extends PreField {
    key?: number;
    pic?: Picture;
    pic_and_cell?: boolean;
}

export function new_field(): Field {
    return Object.fromEntries(Object.keys(borders).map(dir => [dir, false])) as unknown as Field;
}

export type direction = 'horizontal' | 'vertical';

export interface QuestionIdentifier {
    key: number
    direction: direction
}

export enum FieldType {
    cell,
    inner_border,
    inner,
    outer,
}
