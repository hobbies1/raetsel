import {
    direction,
    Entry,
    Field,
    FieldType,
    new_field,
    Picture,
    Position,
    PreQuestion,
    Question,
    Reply,
    Size,
} from "./base_types";
import {extract_answer, in_rect, set_twod, twod_array} from "./helper";

async function http<T>(request: RequestInfo): Promise<T> {
    const response = await fetch(request);
    if (!response.ok) {
        throw "Dieses Kreuzworträtsel ist nicht vorhanden.";
    }
    return await response.json();
}

function record_from_entries(entries: Entry[], key: 'horizontal' | 'vertical'): Record<number, Question> {
    return Object.fromEntries(entries.filter(e => e.hasOwnProperty(key))
        .map(e => ([e.key, {key: e.key, pos: e.position, ...e[key]}]))) as Record<number, Question>;
}


export class Puzzle {
    num: number;
    vertical: Record<number, Question>;
    horizontal: Record<number, Question>;
    field: (Field[])[];
    max: Position;
    solution: string[][];
    private title: string;
    private pictures: { pos: Position, size: Size }[] = [];

    constructor(num: number, entries: Entry[], title: string, image: Picture | undefined) {
        this.num = num;
        this.title = title;
        this.max = Puzzle.get_dimensions(entries);
        // Input is 1-based...
        entries.forEach(e => e.position = {x: e.position.x - 1, y: e.position.y - 1});
        this.horizontal = record_from_entries(entries, 'horizontal');
        this.vertical = record_from_entries(entries, 'vertical');
        this.field = twod_array(this.max);
        this.solution = twod_array(this.max);
        this.create_field(entries);
        this.add_picture(image)
    }

    get_key(start: Position, direction: direction): [number, Position] | undefined {
        const pos = Object.assign({}, start);
        let current;
        while ((current = this.get_field(pos)) !== undefined) {
            if (current.key !== undefined && this[direction].hasOwnProperty(current.key)) {
                return [current.key, pos];
            }
            if (direction === 'horizontal') {
                --pos.x;
                if (this.get_field(pos)?.right ?? true) {
                    // We ran through a wall, there is no key in this direction.
                    return undefined;
                }
            } else {
                --pos.y;
                if (this.get_field(pos)?.bottom ?? true) {
                    // We ran through a wall, there is no key in this direction.
                    return undefined;
                }
            }
        }
        throw `Position ${pos} belongs to no key.`;
    }

    static async get_puzzle(num: number): Promise<Puzzle> {
        const reply = await http<Reply>(`/static/${num}.json`);
        document.title += ' — ' + reply.meta['label'];
        return new Puzzle(num, reply.questions, reply.meta['label'], reply.image);
    }

    get_containing_pic(pos: Position): Picture | undefined {
        for (const pic of this.pictures) {
            if (in_rect(pos, pic.pos, pic.size)) {
                return this.get_field(pic.pos)?.pic;
            }
        }
        return undefined;
    }

    is_var_length(pos: Position): boolean {
        const containing = this.get_containing_pic(pos);
        return containing !== undefined && this.get_field_type(pos) === FieldType.cell;
    }

    get_field_type(pos: Position): FieldType {
        const containing = this.get_containing_pic(pos);
        const field = this.get_field(pos);
        if (containing !== undefined) {
            if (containing.position.x === pos.x) {
                if (field !== undefined && field.pic === undefined) {
                    return FieldType.cell;
                }
                return FieldType.inner_border;
            }
            return FieldType.inner;
        }
        if (field === undefined) {
            return FieldType.outer;
        }
        return FieldType.cell;
    }

    get_field(pos: Position): Field | undefined {
        return this.field[pos.x]?.[pos.y];
    }

    set_field(pos: Position, f: Field): void {
        this.field[pos.x][pos.y] = f;
    }

    update_field(pos: Position, f: Partial<Field>): Field {
        if (this.get_field(pos) === undefined) {
            const obj = new_field();
            this.set_field(pos, obj);
        }
        return Object.assign(this.field[pos.x][pos.y], f);
    }

    private static get_dimensions(entries: Entry[]): Position {
        const ret: Position = {x: 0, y: 0};
        for (const entry of entries) {
            const hor = entry.position.x + (entry.horizontal?.length ?? 0);
            ret.x = Math.max(ret.x, hor);
            const ver = entry.position.y + (entry.vertical?.length ?? 0);
            ret.y = Math.max(ret.y, ver);
        }
        return ret;
    }

    private add_picture(pic?: Picture) {
        if (pic === undefined) {
            return;
        }
        // Still wrongly indexed.
        pic.position.x -= 1;
        pic.position.y -= 1;
        const is_also_cell = this.get_field(pic.position) != undefined;
        this.update_field(pic.position, {pic: pic, pic_and_cell: is_also_cell});
        this.pictures.push({pos: pic.position, size: pic.size});
    }

    private get_borders({x, y}: Position) {
        const ret: Partial<Field> = {};

        if (this.get_field({x, y}) === undefined) {
            return undefined;
        }
        if (this.get_containing_pic({x, y}) !== undefined) {
            return undefined;
        }

        if (this.get_field({x, y: y + 1}) === undefined) {
            ret.bottom = true;
        }
        if (this.get_field({x, y: y - 1}) === undefined) {
            ret.top = true;
        }
        if (this.get_field({x: x + 1, y}) === undefined) {
            ret.right = true;
        }
        if (this.get_field({x: x - 1, y}) === undefined) {
            ret.left = true;
        }
        return ret;
    }

    private set_solution(pos_from_offset: (_: number) => Position, answer: string | undefined, length: number) {
        if (answer === undefined) {
            return;
        }

        if (length === answer.length) {
            for (let i = 0; i < length; ++i) {
                set_twod(this.solution, pos_from_offset(i), answer[i]);
            }
        } else {
            let answer_pos = 0;
            let offset = 0;

            while (offset < length) {
                const containing = this.get_containing_pic(pos_from_offset(offset));
                if (containing === undefined) {
                    set_twod(this.solution, pos_from_offset(offset), answer[answer_pos]);
                    ++offset;
                    ++answer_pos;
                } else {
                    const overflow = answer.length - length + containing.size.width;
                    set_twod(this.solution, pos_from_offset(offset), answer.slice(offset, offset + overflow));
                    offset += containing.size.width;
                    answer_pos += overflow;
                }
            }
        }
    }

    private insert_fields(question: PreQuestion, pos_from_offset: (_: number) => Position, border: Partial<Field>) {
        const answer = question.answer ?? extract_answer(question.description);
        for (let x = 1; x < question.length; ++x) {
            const next_pos = pos_from_offset(x);
            this.update_field(next_pos, {});
        }
        this.update_field(pos_from_offset(question.length - 1), border);
        this.add_picture(question.picture);
        this.set_solution(pos_from_offset, answer, question.length);
        return true;
    }

    private create_field(entries: Entry[]) {
        for (const {key, position: pos, horizontal, vertical} of entries) {
            const next_field: Partial<Field> = {key};
            if (horizontal !== undefined) {
                next_field.left = true;
                this.insert_fields(horizontal, (off: number) => ({x: pos.x + off, y: pos.y}), {right: true});
            }
            if (vertical !== undefined) {
                next_field.top = true;
                this.insert_fields(vertical, (off: number) => ({x: pos.x, y: pos.y + off}), {bottom: true});
            }
            this.update_field(pos, next_field);
        }

        for (let x = 0; x < this.field.length; ++x) {
            for (let y = 0; y < this.field[x].length; ++y) {
                const borders = this.get_borders({x, y});
                if (borders) {
                    this.update_field({x, y}, borders);
                }
            }
        }
    }
}
